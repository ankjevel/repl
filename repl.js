function CMD (socket, msg) {
  repl.start({
    "prompt": msg,
    "input": socket,
    "output": socket
  }).on('exit', function() {
    socket.end();
  });
}

var net = require("net"),
    repl = require("repl"),
    fs = require("fs");

var sock = "/tmp/node-repl-sock",
    port = 6001;

if (fs.existsSync(sock)) {
  fs.unlink(sock, function (error) {
    if (error) {
      throw new Error(error);
    }
  });
}

// PROCESS
repl.start({
  "prompt": "node via stdin> ",
  "input": process.stdin,
  "output": process.stdout
});

// SOCKET
net.createServer(function (socket) {
  new CMD(socket, "node via Unix socket> ");
}).listen(sock);

// PORT
net.createServer(function (socket) {
  new CMD(socket, "node via TCP socket> ");
}).listen(port);